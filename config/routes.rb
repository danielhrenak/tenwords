Rails.application.routes.draw do
  root 'pages#home'
  get 'about' => 'pages#about'

  resources :articles

  get 'signup' => 'users#new'
  #post 'users' => 'users#create'
  resources :users, except: [:new]

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
end
